<?php
/* 
 * Furison's/Alex's custom CMS system
 * bootstrap and initial setup for admin
 * Copyright (c) 2014 Alex Antrobus/Furison
 */

//class autoloader
function classLoader($class)
{
    $classFile = str_replace('_', '/', $class). '.php';
    include_once '../lib/'. $classFile;
}

//exception handler
function exceptionHandler(Exception $ex)
{
    $headFile = '../'. $GLOBALS['settings']->get('file_parts'). '/'
              . $GLOBALS['settings']->get('file_header');
    
    //error page title to put into the template
    $pageTitle = "Something went wrong";
    
    //header
    if(is_file($headFile))
    {
        //get header
        include ($headFile);
        
    }
    else
    {
        echo "<html>\n<head>\n\t<title>$pageTitle</title>\n</head>".
             "\n<body><h1>$pageTitle</h1>";
    }
    
    echo '<h2>Looks like something went wrong...</h2>';
    echo '<p>Error: '. $ex->getMessage() . '<br>';
    if ($GLOBALS['settings']->get('debug') === TRUE)
    {
        echo nl2br($ex);
    }
    echo '</p>';
    
    $footFile = '../'. $GLOBALS['settings']->get('file_parts'). '/'
              . $GLOBALS['settings']->get('file_footer');
    
    if(is_file($footFile))
    {
        //get footer
        include ($footFile);    
    }
    else
    {
        echo '<br>\n</body>\n</html>';
    }
}

//register functions
spl_autoload_register('classLoader');
set_exception_handler('exceptionHandler');

//load site settings
$GLOBALS['settings'] = new Settings();
//get admin settings
include 'admin_settings.php';
//start database connection
$GLOBALS['db'] = new Database($settings->get('database_server'),
                $settings->get('database_user'),
                $settings->get('database_pass'),
                $settings->get('database_database'))

//input sanitiser
function inputSanitser($input, $type, $db)
{
    $input = $db->escape($input);
    settype($input, $type);
    return $input;
}
