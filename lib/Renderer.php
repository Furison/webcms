<?php

/*
 * Furison's/Alex's custom CMS system
 * Page Renderer
 * @copyright 2014 Alex Antrobus/Furison
 */

/**
 * Furison's/Alex's custom CMS system
 * Rendering engine for the CMS. Takes the Markdown for a page, renders it
 * then adds the header and footer, and puts it together
 *
 * @author Alex/Furison
 */
class Renderer {
    
    protected $rendered;
    private $engine;
    
   public function Renderer()
   {
       $this->rendered = false;
       $this->engine = new parsedown_Parsedown();
   }
   
   /**
    * Render the markdown of the page content using $this->engine
    * @param Page $page the page to render
    * @return string The markup of the content
    */
   public function renderContent(Page $page)
   {
       //get page content
       $content = $page->getContent();
       
       //throw at rendering engine
       $markup = $this->engine->text($content);
       
       //return markup
       return $markup;
   }
   
   /**
    * Include the header partial and render any PHP markup
    * @param Page $page the page to render
    * @return string The markup of the header
    * @throws Exception when header template cannot b found
    */
   public function renderHeader(Page $page)
   {
       if(is_file($page->getHeaderFile()))
       {
            //start the output buffer
            ob_start();
            
            //set page title
            $pageTitle = $page->getTitle();

            //get header
            include ($page->getHeaderFile());

            $headData = ob_get_contents();

            //close output buffer
            ob_end_clean();
       
            return $headData;
       }
       else
       {
           throw new Exception('Page header file not found');
       }
   }
   
   /**
    * Include the footer file and render any PHP markup
    * @param Page $page The page to render
    * @return string The markup of the footer
    * @throws Exception When footer file cannot be found
    */
   public function renderFooter(Page $page)
   {
       if(is_file($page->getFooterFile()))
       {
            //start the output buffer
            ob_start();

            //get header
            include ($page->getFooterFile());

            $footData = ob_get_contents();

            //close output buffer
            ob_end_clean();

            return $footData;
       }
       else
       {
           throw new Exception('Footer file cannot be found');
       }
   }
   
   public function renderPage(Page $page)
   {
       echo $this->renderHeader($page);
       echo $this->renderContent($page);
       echo $this->renderFooter($page);
       $this->rendered = true;
   }
   
   public function renderPageNoFoot(Page $page)
   {
       echo $this->renderHeader($page);
       echo $this->renderContent($page);
       $this->rendered = true;
   }
   
   public function renderPageNoHead(Page $page)
   {
       echo $this->renderContent($page);
       echo $this->renderFooter($page);
       $this->rendered = true;
   }
}