<?php

/*
 * Furison's/Alex's Custom CMS system
 * Configuration settings for the CMS system
 * @copyright 2014 Alex Antrobus/Furison
 */

/**
 * Configuration settings for the CMS system
 * Contains an associatve array of global settings
 * 
 * @author Alex
 * @copyright (c) 2014, Alex Antrobus/Furison
 */
class Settings
{
    private $settings = array(
        #localhost dev settings
        ##database settings
        'db_server'     => '127.0.0.1',
        'db_user'       => 'webserver',
        'db_pass'       => 'furison',
        'db_database'   => 'webserver',
        #general settings
        'site_name'     => 'Furison\'s Homepage',
        'debug'         => true,
        #database settings
//        'db_server'     => '127.0.0.1',
//        'db_user'       => 'user',
//        'db_pass'       => 'pass',
//        'db_database'   => 'database',
        #file locations
        'file_parts'    => 'partials',
        'file_header'   => 'header.html',
        'file_footer'   => 'footer.html',
        #admin
        'admin_timeout' => 3600
    );
    
    /**
     * Fetch a setting using the key required
     * @param String $setting the key to find the setting of
     * @return mixed
     * @throws Exception when the key could not be found
     */
    public function get($setting)
    {
        if(!array_key_exists($setting, $this->settings))
        {
            throw new Exception($setting.' is not a valid key');
        }
        else
        {
            return $this->settings[$setting];
        }
    }#end function
}