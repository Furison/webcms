<?php

/*
 * Furison's/Alex's custom CMS system
 * Page class
 * @copyright 2014 Alex Antrobus/Furison
 */

/**
 * Furison's/Alex's custom CMS system
 * Contains information about a page
 *
 * @author Alex/Furison
 */
class Page {
    
    public $id;
    private $pageSlug;
    private $title;
    private $content;
    private $created;
    private $lastUpdate;
    private $author;
    private $headerFile;
    private $footerFile;
    
    public function Page($pageId = 'index')
    {
        global $g_settings;
        //grab the db
        $db = $GLOBALS['g_db'];
        
        //get the page data
        $db->select(array('*'), 'page', $pageId);
        
        //populate page data
        $this->id = $db->get('id');
        $this->pageSlug = $db->get('slug');
        $this->title = $db->get('title');
        $this->content = $db->get('content');
        $this->author = $db->get('author');
        $this->lastUpdate = $db->get('last_update');
        $this->created = $db->get('created');
        
        //header and footer
        $folder = $GLOBALS['settings']->get('file_parts'). '/';
        $this->headerFile = $folder . $GLOBALS['settings']->get('file_header');
        $this->footerFile = $folder . $GLOBALS['settings']->get('file_footer');
     }
     
     public function getId()
     {
         return $this->id;
     }
     public function getContent()
     {
         return $this->content;
     }
     
     public function setContent($content)
     {
         $this->content = $content;
     }
     
     public function getTitle()#
     {
         return $this->title;
     }
     
     public function getAuthor()
     {
         return $this->author;
     }
     
     public function getLastUpdate()#
     {
         return $this->lastUpdate;
     }
     
     public function getHeaderFile()
     {
         return $this->headerFile;
     }
     
     public function getFooterFile()
     {
         return $this->footerFile;
     }
}
