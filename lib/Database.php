<?php

/**
 * Simple database absrtaction layer for cms project
 * @author Alex Antrobus/Furison
 * @copyright 2014 Alex Antrobus
 */
##TODO: change over to MySQLi interface

class Database
{
	const WHERE_MODE_AND = 'AND';
	const WHERE_MODE_OR  = 'OR';
	const WHERE_MODE_NOT = 'NOT';

	private $p_connection;
	private $p_lastQuery;
	private $p_resultSet;
	private	$p_queryRes;
        private $p_rows;
        private $whereMode;
	
        public function Database($server, $user, $pass, $database)
        {
            //connect to sever
            $this->connect($server, $user, $pass, $database);
            
            //initialise variables
            $this->p_lastQuery = '';
            $this->p_queryRes = null;
            $this->p_resultSet = array();
            //default where mode
            $this->whereMode = self::WHERE_MODE_AND;
        }
        
        
	public function select($columns, $table, $where = 1, 
					$mode = self::WHERE_MODE_AND)
	{
		//prepare columns
		$cols = implode(',', $columns);

		//prepare where
		if (is_array($where))
		{
			$procWhere = implode($mode, $where);
		}
		else
		{
			$procWhere = $where;
		}
		
		//prepare statement
		$queryStr = 'SELECT '. $cols .' FROM ' . $table . ' WHERE '
			. 'id=1';//$procWhere;
//print $queryStr;
		//execute Query
		$this->p_queryRes = $this->p_connection->query($queryStr);

		$this->nextResult();
                $this->p_lastQuery = $queryStr;
                $this->p_rows = $this->p_connection->affected_rows;
        }
        
        public function connect($server, $user, $password, $database)
        {
            $link = new mysqli($server, $user, $password, $database);
            if(!$link)
            {
                throw new Exception('Cannot connect to database: '. 
                        mysqli_connect_error(), mysqli_connect_errno());
            }
            else
            {
                
                if(!true)
                {
                    throw new Exception('Cannot select database: '. 
                            mysqli_connect_error(), mysqli_connect_errno());
                }
                else
                {
                    $this->p_connection = $link;
                }
            }
        }
        
        /**
         * gets a specific value from a database result set
         * @param string $key the key to look for
         * @return mixed the value of the result
         * @throws Exception when given a non-existing key
         */
        public function get($key)
        {
            if (!array_key_exists($key, $this->p_resultSet))
            {
                throw new Exception( $key . ' is not a valid field');
            }
            else
            {
                return $this->p_resultSet[$key];
            }
        }
        
        public function nextResult()
        {
            if(!$this->p_queryRes)
		{
			throw new Exception("Cannot fetch data: ". 
                                $this->p_connection->error,
				$this->p_connection->errno);
                        
		}
		else
		{
			//fetch results
			$this->p_resultSet = $this->p_queryRes->fetch_assoc();
                }
        }
        
        public function escape($string)
        {
            return mysqli_real_escape_string($this->p_connection, $string);
        }
        
        public function count_rows()
        {
            return $this->p_rows;
        }
    
}