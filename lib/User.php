<?php

/*
 * Furison's/Alex's custom CMS system
 * User class
 * Copyright (c) 2015 Alex Antrobus/Furison
 */

/**
 * User class
 *
 * @author alex
 */
class User {
    
   const USER_NOT_STATIC = -1;
   private $id;
   private $name;
   private $email;
   private $level;
   private $last_login;
   
   public function User($userID = User::USER_NOT_STATIC)
   {
       if($userID == User::USER_NOT_STATIC)
       {
           //assume blank user
           $this->id = User::USER_NOT_STATIC;
           $this->name = '';
           $this->email = 'noone@example.com';
           $this->level = 0;
           $this->last_login = new DateTime(0);
        }
        else
        {
            $this->id = $userID;
            $this->loadUser();
        }
   }
   
   public function loadUser($db_conn)
   {
       if($this->id == User::USER_NOT_STATIC || !$this->id)
       {
           throw new Exception("Invalid user ID");
       }
       
       //find user
       $db_conn->select(array('*'), 'user', 'WHERE id = '. $this->id)
       if($db_conn->count_rows() == 1)
       {
            //populate object
            $this->name = $db_conn->get('name');
            $this->email = $db_conn->get('email');
       }
   }
}
