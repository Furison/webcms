<?php
/*
 * Furison's/Alex's Custom CMS system
 * main script page
 * @copyright 2014 Alex Antrobus/Furison
 */
//get script start time
$GLOBALS['start_time'] = microtime(true);

//get bootstrap
include_once 'bootstrap.php';


//connect DB
$g_db = new Database($GLOBALS['settings']->get('db_server'),
                     $GLOBALS['settings']->get('db_user'), 
                     $GLOBALS['settings']->get('db_pass'),
                     $GLOBALS['settings']->get('db_database'));

//get page
//$g_page = null;
if(isset($_REQUEST['page']))
{
    //sanitise input
    $pageId = inputSanitser($_REQUEST['page'], 'integer', $g_db);
    $g_page = new Page($pageId);
}
else 
{
    $g_page = new Page();
}

//create renderer instnace
$g_renderer = new Renderer();
//render page
//start OB so we can catch errors
ob_start();
//this renders the full page in one go
$g_renderer->renderPage($g_page);

//while this renders the page without the footer
//$g_renderer->renderPageNoFoot($g_page);

//finally we render the page footer with the timer
//echo $g_renderer->renderFooter($g_page);

//output the buffered page
ob_end_flush();